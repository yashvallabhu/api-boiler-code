Add a .env file at the root of the project

# .env file should contain:
``` 
PORT = 
DB_URL = 
JWT_KEY = 
EMAIL_ADDRESS = 
EMAIL_PASSWORD = 
```

# Download the packages using: npm install
