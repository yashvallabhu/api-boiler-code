const express = require("express");
const app = express();
const morgan = require('morgan');

const userRouter = require('./v1/routes/user_router.js');

app.use(morgan('tiny'));

app.use(express.urlencoded({extended:false}));;
app.use(express.json())

app.use('/users', userRouter);

app.use((req, res, next)=>{
	const err = new Error("not found")
	err.status = 404
	next(err)
})

app.use((error, req, res, next)=>{
	return res.status(error.status || 500).json({
		error: {
			message: error.message
		}
	})
})

module.exports = app