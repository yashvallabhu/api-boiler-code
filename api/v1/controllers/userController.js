const sanitize = require('mongo-sanitize');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken")
const config = require("../../../config/config");
const User = require("../models/user");

module.exports.register = async (req, res, next) => {
    try{
        const clean = sanitize(req.body);
        const { username, password, email} = clean;
        
        if (!(username && password && email)){
            var err = new Error('Username, password, email are required!');
            err.status = 400;
            throw err;
        }

        const exusers = await User.find({$or : [
            {'username': username},
            {'email' : email, 'isVerified' : true}
        ]})

        if(exusers.length > 0){
            var err = new Error('A user with the given username or email already exists');
            err.status = 409;
            throw err;
        }

        const encrypt = await bcrypt.hash(String(password), 10);

        const user = new User({
            username : username,
            hash : encrypt,
            email : email
        })

        await user.save();
        return res.status(201).json({
            'message' : 'Account is successfully created!',
            "user_id" : user.id,
            'username' : user.username
        });
    }
    catch(e){
        next(e);
    }
}

module.exports.login = async (req, res, next) => {
    try{
        if(req.user){
            return res.status(200).send("You're already logged in");
        }
        const clean = sanitize(req.body);
        const { username, password } = clean;

        const user = await User.findOne({username : username});
    
        if(user && bcrypt.compare(password, user.hash)){
            const token = jwt.sign({
                user_id : user.id,
                email: user.email
            },
            config.jwt.KEY, {
                expiresIn : config.jwt.EXPIRY_TIME
            })

            return res.status(200).json({
                "Success" : true,
                "message" : "Use this as a Bearer token",
                "token" : token
            })
        }

        var err = new Error("Invalid credentials");
        err.status = 400;
        throw err;
    }
    catch(e){
        next(e);
    }
}

module.exports.users = async (req, res, next) => {
    try{
        const users = await User.find().select('_id username');
        return res.status(200).json(users);
    }
    catch(e){
        next(e);
    }
}