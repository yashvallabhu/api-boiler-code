const jwt = require("jsonwebtoken");
const config = require("../../../config/config");
const key = config.jwt.KEY;
const port = config.app.PORT;

module.exports = (req, res, next)=>{
	try{
		const token = req.headers.authorization.split(" ")[1];
		const details = jwt.verify(token, key);
		req.user = details;
		next();
	}
	catch(err){
        return res.status(401).json({
			message: "Authorization failed",
			url: `http://localhost:${port}/users/login`
		})
	}
}