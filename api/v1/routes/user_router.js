const express = require("express");
const router = express.Router();

const controller = require("../controllers/userController")
const auth = require("../middlewares/auth.js");

router.post("/register", controller.register);
router.post("/login", controller.login);
router.get("/", controller.users);

router.use(auth);

module.exports = router