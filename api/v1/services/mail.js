const nodemailer = require('nodemailer');
const config = require("../../../config/config");
  
let mailTransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.mail.email,
        pass: config.mail.password
    }
});
  
module.exports = mailTransporter;