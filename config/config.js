module.exports = {
    app : {
        PORT : process.env.PORT || 3000
    },
    db : {
        PORT : process.env.DB_PORT || 27017,
        URL : process.env.DB_URL || "mongodb://localhost:27017/flipr"
    },
    jwt : {
        KEY : process.env.JWT_KEY || "hfubwaAJBCJBVB",
        EXPIRY_TIME : process.env.JWY_EXPIRY_TIME || '1d'
    },
    mail : {
        email : process.env.EMAIL_ADDRESS,
        password : process.env.EMAIL_PASSWORD
    }
}